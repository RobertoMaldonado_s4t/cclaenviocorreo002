'use strict';
const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const cors = require('cors')({ origin: true });
const crypto = require('crypto');
const moment = require('moment-timezone');


/*
if (process.env.NODE_ENV != "production") {
    require("dotenv").config();
}
*/

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}

function getFechaHora() {

    var fechaHora = moment().tz("America/Santiago").format('DD-MM-YYYY HH:mm');
    console.log('moment: ' + fechaHora);

    /*
    var date = new Date();
    var fechaHora = addZero(date.getDate()) + "/" + addZero(date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
    */

    return fechaHora;

}

function getAlgorithm(keyBase64) {

    var key = Buffer.from(keyBase64, 'base64');
    switch (key.length) {
        case 16:
            return 'aes-128-cbc';
        case 32:
            return 'aes-256-cbc';
    }
    throw new Error('Invalid key length: ' + key.length);
}

function decrypt(messagebase64, keyBase64, ivBase64) {

    try {

        var key = Buffer.from(keyBase64, 'base64');
        var iv = Buffer.from(ivBase64, 'base64');
        const decipher = crypto.createDecipheriv(getAlgorithm(keyBase64), key, iv)
        return decipher.update(messagebase64, 'base64', 'utf8') + decipher.final('utf8')

    } catch (err) {

        return err;

    }

}

/*
function decrypt(messagebase64, keyBase64, ivBase64) {

    try {

        let binaryData = Buffer.from(messagebase64, "base64");
        let base64Dec = binaryData.toString("utf8");
        const key = Buffer.from(keyBase64, 'base64');
        const iv = Buffer.from(ivBase64, 'base64');

        const decipher = crypto.createDecipheriv(getAlgorithm(keyBase64), key, iv);

        let decrypted = decipher.update(base64Dec, 'base64');
        decrypted += decipher.final();
        return decrypted;

    } catch (err) {

        var respuesta = {
            error: true,
            codigo: 500,
            mensaje: err.message,
            usuario: null
        }

        return res.send({ respuesta });
    }

}
*/


// Nodejs encryption with CTR


function getDecodificar(value) {

    if (value == null) {
        return '';
    }

    var buff = new Buffer(value, 'base64');
    var text = buff.toString('ascii');

    return text;
}

const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: "noresponderccla@gmail.com",
        pass: "s4t32020"
    }
});

exports.enviarEmail = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        const { body } = req;
        const isValidMessage = body.email && body.tipoAccion && body.nombre && body.motivoEnvio && body.tipoDocumento && body.cantDocumento;

        if (!isValidMessage) {

            var respuesta = {
                error: true,
                codigo: 400,
                mensaje: 'parametros de entrada invalidos'
            }

            return res.send({ respuesta });
        }

        if (!body.observacion) {
            body.observacion = "";
        }

        //personalizar html
        if (body.tipoAccion === "1") {
            var titulo = "Subida de documentos realizada con éxito";
            var subTitulo = "Sus documentos se encuentran almacenados en nuestro sistema, a la espera de ser revisados por uno de nuestros ejecutivos.";
        } else if (body.tipoAccion === "2") {
            var titulo = "Eliminación de documentos realizada con éxito";
            var subTitulo = "";
        } else if (body.tipoAccion === "3") {
            var titulo = "Sus documentos fueron aprobados";
            var subTitulo = "Sus documentos fueron revisados y aprobados por uno de nuetros ejecutivos.";
        } else if (body.tipoAccion === "4") {
            var titulo = "Sus documentos fueron rechazados";
            var subTitulo = "Sus documentos fueron revisados y rechazados por uno de nuetros ejecutivos, por favor revise las observaciones y vuelva a subir los documentos.";
        } else {
            var titulo = "";
            var subTitulo = "";
        }

        var informacion = { titulo: titulo, subTitulo: subTitulo };
        var fechaHora = getFechaHora();

        const htmlPersonalizado = `<html>  
                                        <head>  
                                            <meta charset='UTF-8'>  
                                            <style>  
                                                .mainBox {  
                                                width: 647px;  
                                                font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;  
                                                color: #737B7B;  
                                                font-size: 12pt;  
                                                }  
                                                .banner {  
                                                height: 80;  
                                                background: url('https://qawp.cajalosandes.cl/cs/groups/public/documents/digitalmedia/axat/y29y/~edisp/footer-bip-correo.jpg') no-repeat;  
                                                }  
                                                .imgSlider {  
                                                text-align: center;  
                                                }  
                                                h3 {  
                                                font-size: 16pt;  
                                                font-weight: 600;  
                                                color: #0076A9;  
                                                }  
                                                .cont {  
                                                text-align: center;  
                                                }  
                                                .cont p {  
                                                text-align: center;  
                                                font-weight: 500;  
                                                }  
                                                table {  
                                                width: 95%;  
                                                margin: 0 auto;  
                                                }  
                                                .cont table td {  
                                                background: #F5F5F5;  
                                                border: #ffffff;  
                                                padding: 5px;  
                                                font-size: 12pt;  
                                                }  
                                                .cont .headTable {  
                                                background: #0076A9;  
                                                color: #ffffff;  
                                                font-size: 16pt;  
                                                text-align: center;  
                                                font-weight: 600;  
                                                }  
                                                ul {  
                                                list-style: none;  
                                                }  
                                                li {  
                                                color: #0076A9;  
                                                text-align: left;  
                                                }  
                                                ul li::before {  
                                                content: '?';  
                                                color: #F9BE00;  
                                                font-weight: bold;  
                                                display: inline-block;  
                                                width: 1.2em;  
                                                margin-left: -1em;  
                                                }  
                                                a {  
                                                text-decoration: none;  
                                                }  
                                                .btnMasInfo {  
                                                background: #F9BE00;  
                                                padding-left: 10px;  
                                                padding-right: 10px;  
                                                padding-top: 5px;  
                                                padding-bottom: 5px;  
                                                color: #ffffff;  
                                                font-weight: 500;  
                                                font-size: 16pt;  
                                                margin: 0 auto;  
                                                -webkit-border-radius: 5px;  
                                                -moz-border-radius: 5px;  
                                                border-radius: 5px;  
                                                }  
                                                .footer {  
                                                margin-top: 20px;  
                                                height: 15%;  
                                                background: url('https://qawp.cajalosandes.cl/cs/groups/public/documents/digitalmedia/axat/y29y/~edisp/footer-bip-correo.jpg') no-repeat;  
                                                padding-top:10px;  
                                                }  
                                                .footer table {  
                                                color: #ffffff;  
                                                }  
                                            </style>  
                                            
                                            <script>
                                                var value = '1';
                                            </script>
                                            </head>  
                                            <body>  
                                            <div class='mainBox'>  
                                                <a href='https://www.cajalosandes.cl/'> 
                                                <div class='banner'>  
                                                </div>  
                                                </a>   
                                                <div class='imgSlider'>  
                                                <h3>${informacion.titulo}</h3>
                                                </div>  
                                                <div class='cont'>  
                                                <p>${informacion.subTitulo}</p>
                                                <table>  
                                                    <tr>  
                                                    <td colspan='2' class='headTable'>Información</td>  
                                                    </tr>  
                                                    <tr>  
                                                    <td>Afiliado</td>  
                                                    <td>${body.nombre}</td>  
                                                    </tr>  
                                                    <tr>  
                                                    <td>Motivo de Envió</td>  
                                                    <td>${body.motivoEnvio}</td>  
                                                    </tr>  
                                                    <tr>  
                                                    <td>Tipo de Documento</td>  
                                                    <td>${body.tipoDocumento}</td>  
                                                    </tr>  
                                                    <tr>  
                                                    <td>Cantidad de Documentos</td>  
                                                    <td>${body.cantDocumento}</td>  
                                                    </tr>  
                                                    <tr>  
                                                    <td>Observación</td>  
                                                    <td>${body.observacion}</td>
                                                    </tr>
                                                    <tr>  
                                                    <td>Fecha y Hora</td>  
                                                    <td>${fechaHora}</td>  
                                                    </tr>  							
                                                </table>  
                                            
                                                <ul>  
                                                    <h3 style='text-align: left;'>Otros puntos</h3>  
                                                    <li><a href='https://www.cajalosandes.cl/punto-caja'>Visita tu sucursal</a></li>  
                                                </ul>  
                                                <h3>Sucursal Virtual Caja los Andes</h3>  
                                                <a href='https://www.cajalosandes.cl' class='btnMasInfo'> Mas Información</a> 
                                                </div>  
                                                <div class='footer'>  
                                                </div>  
                                            </div>  
                                        </body>  
                                    </html>`;

        const email = {
            from: `"Caja Los Andes " <noresponderccla@gmail.com>`,
            to: body.email,
            subject: "Información sobre el estado de sus documentos",
            message: "",
            html: htmlPersonalizado
        };

        transporter.sendMail(email, (error, info) => {
            if (error) {

                var respuesta = {
                    error: true,
                    codigo: 500,
                    mensaje: err.message
                }

                return res.send({ respuesta });
            }

            var respuesta = {
                error: false,
                codigo: 200,
                mensaje: 'email enviado'
            };

            return res.send({ respuesta });
        });
    });
});

exports.desencriptar = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        const { body } = req;
        const isValidMessage = body.parametros;

        if (!isValidMessage) {

            var respuesta = {
                error: true,
                codigo: 400,
                mensaje: 'parametros de entrada invalidos',
                usuario: null
            }

            return res.send({ respuesta });
        }

        var keyBase64 = "QmFyMTIzNDVCYXIxMjM0NQ==";
        var ivBase64 = "UmFuZG9tSW5pdFZlY3Rvcg==";

        var parametrotDes = decrypt(getDecodificar(body.parametros), keyBase64, ivBase64);

        if (typeof(parametrotDes) === 'object') {

            var respuesta = {
                error: true,
                codigo: 500,
                mensaje: 'error al desencriptar',
                usuario: usuario
            };

            return res.send({ respuesta });
        }

        var usuario = JSON.parse(parametrotDes);

        var respuesta = {
            error: false,
            codigo: 200,
            mensaje: 'desencriptacion correcta',
            usuario: usuario
        };

        return res.send({ respuesta });
    });
});